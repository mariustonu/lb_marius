describe('Maintenance', function () {
    it('System Control', function () {
        cy.get('a').contains('Maintenance').click();
        cy.get('a').contains('System Control').click();
        cy.get('form[name=shutdown]').children('input[type=submit][name=go]').click();
    })
})