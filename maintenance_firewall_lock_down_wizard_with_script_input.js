describe('Maintenance', function () {
    it('Firewall Lock Down Wizard', function () {
        cy.get('a').contains('Maintenance').click();
        cy.get('a').contains('Firewall Lock Down Wizard').click();
        cy.get('input[type=text][name=admin_net]').clear();
        cy.get('input[type=text][name=admin_net]').type('10.9.0.178/24');
        cy.get('a').contains('Modify the firewall lock down wizard script').click()
        cy.get('textarea').type(' {uparrow} {enter} iptable command1 {enter} {enter} iptables command2 ')
        cy.get('input[type=submit][name=go]').click()
        cy.get('a').contains('Firewall Lock Down Wizard').click()
        cy.get('input[type=checkbox][value=on]').click()
        cy.get('input[type=submit][name=go]').click()
    })
})