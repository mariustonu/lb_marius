describe('System Overview', function () {
    it('System Overview', function () {
        cy.get('a').contains('System Overview').click();
        cy.get('a').contains('Accept').click();
        cy.get('input[type=text][name=virtual_label]').clear();
        cy.get('input[type=text][name=virtual_label]').type('Cypress VIP1');
        cy.get('input[type=text][name=virtual_ip]').clear();
        cy.get('input[type=text][name=virtual_ip]').type('192.168.93.68');
        cy.get('input[type=text][name=virtual_ports]').clear();
        cy.get('input[type=text][name=virtual_ports]').type('80');
//use input[id instead of type to bypass multiple oject error on click
        cy.get('input[id=virtual_submit][name=go]').click();
//use {force: true } to bypass hidden objects error on populating fields
        cy.get('input[type=text][name=real_label_0]').type('debian', { force: true });
        cy.get('input[type=text][name=real_ip_0]').type('192.168.93.220', { force: true });
//?RIP Always attaches to a previous existing VIP?
        cy.get('input[type=text][name=real_port_0]').type('80', { force: true });
        cy.get('input[id=real_submit][name=go]').click();
//goes back to system overview
        cy.get('a').contains('System Overview').click()
//clicks another option so the Reload HAProxy blue box appears
        cy.get('a').contains('Local Configuration').click();
        cy.get('a').contains('System Overview').click()
        cy.get('input[id=haproxy]').click();
    })    
})