describe('Logs', function () {
    it('Load Balancer', function () {
        cy.get('a').contains('Logs').click();
        cy.get('a').contains('Load Balancer').click();
        cy.get('input[type=text][name=search_logs]').clear();
        cy.get('input[type=text][name=search_logs]').type('lbmaster');
        cy.get('input[type=submit][value=Search]').click();
    })    
})
 