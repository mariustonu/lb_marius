describe('Local Configuration', function () {
    it('Routing', function () {
        cy.get('a').contains('Local Configuration').click();
        cy.get('a').contains('Routing').click();
        cy.get('input[type=text][name=network_0]').clear();
        cy.get('input[type=text][name=network_0]').type('192.168.93.0/19');
        cy.get('input[type=text][name=gateway_0]').clear();
        cy.get('input[type=text][name=gateway_0]').type('192.168.93.1');
        cy.get('input[type=submit][name=go]').click();
    })    
})
 