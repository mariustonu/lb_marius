describe('Local Configuration', function () {
    it('Execute Shell Command', function () {
        cy.get('a').contains('Local Configuration').click();
        cy.get('a').contains('Execute shell command').click();
        cy.get('input[type=text][name=command]').clear();
        cy.get('input[type=text][name=command]').type('ip a');
        cy.get('input[type=submit][name=go]').click();
    })    
})
 